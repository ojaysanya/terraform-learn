provider "aws" {
    region = "us-east-1"
}

variable vpc_cidr_block {}
variable subnet_cidr_block {}
variable avail_zone {}
variable env_prefix{}
variable my_ip{}
variable instance_type{}
variable my_public_key{}

resource "aws_vpc" "myapp_vpc" {
    cidr_block = var.vpc_cidr_block
    tags = {
        Name = "${var.env_prefix}_vpc"
    }

}

resource "aws_subnet" "myapp_subnet_1"{
    vpc_id = aws_vpc.myapp_vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.avail_zone
    tags = {
        Name = "${var.env_prefix}_subnet_1"
    }
}

# resource "aws_route_table" "myapp_route_table" {
#     vpc_id = aws_vpc.myapp_vpc.id
#     route {
#          cidr_block = "0.0.0.0/0"
#          gateway_id = aws_internet_gateway.myapp_igw.id
#     }
#     tags = {
#         Name = "${var.env_prefix}_rtb"
#     }
# }

resource "aws_internet_gateway" "myapp_igw" {
    vpc_id = aws_vpc.myapp_vpc.id
    tags = {
        Name = "${var.env_prefix}_igw"
    }

}

resource "aws_default_route_table" "main_rtb" {
    default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id 
    route {
         cidr_block = "0.0.0.0/0"
         gateway_id = aws_internet_gateway.myapp_igw.id
    }
    tags = {
        Name = "${var.env_prefix}_main_rtb"
    }
}

resource "aws_default_security_group" "default_sg" {

    vpc_id = aws_vpc.myapp_vpc.id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my_ip]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
    
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"] 
        prefix_list_ids = []
    }

    tags = {
        Name = "${var.env_prefix}_default_sg"
    }
}

# data "aws_ami" "latest_amazon_linux_image" {
#     most_recent = true
#     owners = ["amazon"]
#     filter {
#         name = "name"
#         values = ["Amazon Linux 2 Kernel 5.10 AMI 2.0.20221210.1 x86_64 HVM gp2"]
#     }
#     filter {
#         name = "virtualization-type"
#         values = ["hvm"]
#     }
# }
output "ec2_public_ip" {
    value = aws_instance.myapp_server.public_ip
  
}

resource "aws_key_pair" "ssh_key" {
    key_name = "server_key"
    public_key = var.my_public_key 
  
}
# output "aws_ami_id" {
#      value = data.aws_ami.latest_amazon_linux_image
# }
resource "aws_instance" "myapp_server" {
    ami = "ami-0b5eea76982371e91"
    instance_type = var.instance_type

    subnet_id = aws_subnet.myapp_subnet_1.id
    vpc_security_group_ids = [aws_default_security_group.default_sg.id]
    availability_zone = var.avail_zone

    associate_public_ip_address = true
    key_name = aws_key_pair.ssh_key.key_name

    user_data = <<EOF
                    #!/bin/bash
                    sudo yum update -y && sudo yum install -y docker
                    sudo systemctl start docker
                    sudo usermod -aG docker ec2-user
                    docker run -p 8080:80 nginx
                    EOF

    tags = {
        Name = "${var.env_prefix}_server"
    }
  
}